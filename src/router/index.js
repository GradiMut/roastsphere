import Vue from 'vue'
import Router from 'vue-router'
import index from '@/components/index'
import food from '@/components/menu/food'
import foodCategory from '@/components/menu/foodCategory'
import dessert from '@/components/menu/dessert'
import dessertCategory from '@/components/menu/dessertCategory'
import beverage from '@/components/menu/beverage'
import beverageCategory from '@/components/menu/beverageCategory'
import editFood from '@/components/Edit/editFood'
import editDessert from '@/components/Edit/editDessert'
import editBev from '@/components/Edit/editBev'
import order from '@/components/order'
import notfication from '@/components/notification'
import reservation from '@/components/reservation'
import inbox from '@/components/inbox'
import review from '@/components/review'
import login from '@/components/login'
import AuthGuard from './auth-guard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: index,
      beforeEnter: AuthGuard
    },
    {
      path: '/menu/food/:id',
      name: 'food',
      component: food,
      beforeEnter: AuthGuard
    },
    {
      path: '/menu/foodCategory',
      name: 'foodCategory',
      component: foodCategory,
      beforeEnter: AuthGuard
    },
    {
      path: '/menu/dessert/:id',
      name: 'dessert',
      component: dessert,
      beforeEnter: AuthGuard
    },
    {
      path: '/menu/dessertCategory',
      name: 'dessertCategory',
      component: dessertCategory,
      beforeEnter: AuthGuard
    },
    {
      path: '/menu/beverage/:id',
      name: 'beverage',
      component: beverage,
      beforeEnter: AuthGuard
    },
    {
      path: '/menu/beverageCategory',
      name: 'beverageCategory',
      component: beverageCategory,
      beforeEnter: AuthGuard
    },
    {
      path: '/Edit/editFood/:id',
      name: 'editFood',
      component: editFood,
      beforeEnter: AuthGuard
    },
    {
      path: '/Edit/editDessert/:id',
      name: 'editDessert',
      component: editDessert,
      beforeEnter: AuthGuard
    },
    {
      path: '/Edit/editBev/:id',
      name: 'editBev',
      component: editBev,
      beforeEnter: AuthGuard
    },
    {
      path: '/order',
      name: 'order',
      component: order,
      beforeEnter: AuthGuard
    },
    {
      path: '/notfication',
      name: 'notfication',
      component: notfication,
      beforeEnter: AuthGuard
    },
    {
      path: '/reservation',
      name: 'reservation',
      component: reservation,
      beforeEnter: AuthGuard
    },
    {
      path: '/inbox',
      name: 'inbox',
      component: inbox,
      beforeEnter: AuthGuard
    },
    {
      path: '/review',
      name: 'review',
      component: review,
      beforeEnter: AuthGuard
    },
    {
      path: '/login',
      name: 'login',
      component: login
    }
  ]
})
