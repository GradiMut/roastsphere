import * as firebase from 'firebase'

export default {
  state: {
    reservationSize: null
  },
  mutations: {
    setReservationSize (state, payload) {
      state.reservationSize = payload
    }
  },
  actions: {
    fetchResrervationSize ({commit}, payload) {
      firebase.firestore().collection('counters').doc('reservations').onSnapshot(doc => {
        const reservationSize = doc.data().reservationsCount
        commit('setReservationSize', reservationSize)
      })
    }
  },
  getters: {
    reservationSize (state) {
      return state.reservationSize
    }
  }
}
