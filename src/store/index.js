import Vue from 'vue'
import Vuex from 'vuex'

import inbox from './inbox'
import menus from './menus'
import order from './order'
import reservation from './reservation'
import review from './review'
import user from './user'
import utils from './utils'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    inbox: inbox,
    menus: menus,
    order: order,
    reservation: reservation,
    review: review,
    user: user,
    utils: utils
  }
})
