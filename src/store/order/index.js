import * as firebase from 'firebase'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default {
  state: {
    order: [],
    orderSize: null
  },
  mutations: {
    setOrder (state, payload) {
      state.order = payload
    },
    setOrderSize (state, payload) {
      state.orderSize = payload
    }
  },
  actions: {
    fetchOrder ({commit}, payload) {
      firebase.firestore().collection('orders').orderBy('orderedAt', 'desc').onSnapshot(function (querySnapshot) {
        const orders = []
        querySnapshot.forEach(function (doc) {
          const obj = doc.data()
          orders.push(obj)
        })
        commit('setOrder', orders)
      })
    },
    fetchOrderSize ({commit}, payload) {
      firebase.firestore().collection('counters').doc('orders').onSnapshot(doc => {
        const orderSize = doc.data().ordersCount
        commit('setOrderSize', orderSize)
      })
    }
  },
  getters: {
    orderSize (state) {
      return state.orderSize
    },
    getOrder (state) {
      return state.order
    }
  }
}
