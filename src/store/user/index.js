import * as firebase from 'firebase'
import router from '@/router'

export default {
  state: {
    user: null
  },
  mutations: {
    setUser (state, payload) {
      state.user = payload
    }
  },
  actions: {
    signUserIn ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password).then(firebaseUser => {
        let auth = firebase.auth().currentUser.uid
        commit('setLoading', false)
        commit('setUser', auth)
        router.push('/')
      }).catch(error => {
        commit('setLoading', false)
        commit('setError', error)
      })
    },
    autoSignIn ({commit}, payload) {
      commit('setUser', {
        user: payload.uid
      })
    },
    userSignOut ({commit}) {
      firebase.auth().signOut()
      commit('setUser', null)
      router.push('/')
    }
  },
  getters: {
    user (state) {
      return state.user
    }
  }
}
