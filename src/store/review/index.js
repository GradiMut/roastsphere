import * as firebase from 'firebase'

export default {
  state: {
    reviewSize: null
  },
  mutations: {
    setReviewSize (state, payload) {
      state.reviewSize = payload
    }
  },
  actions: {
    fetchReviewSize ({commit}, payload) {
      firebase.firestore().collection('counters').doc('reviews').onSnapshot(doc => {
        const reviewSize = doc.data().reviewsCount
        commit('setReviewSize', reviewSize)
      })
    }
  },
  getters: {
    reviewSize (state) {
      return state.reviewSize
    }
  }
}
