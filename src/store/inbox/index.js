import * as firebase from 'firebase'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default {
  state: {
    message: null
  },
  mutations: {
    setMessage (state, payload) {
      state.message = payload
    }
  },
  actions: {
    fetchMessage ({commit}, payload) {
      firebase.firestore().collection('counters').doc('messages').onSnapshot(function (doc) {
        if (doc.exists) {
          const messageSize = doc.data().messagesCount
          commit('setMessage', messageSize)
        } else {
          console.log('No documeent found for message')
        }
      })
    }
  },
  getters: {
    getMessage (state) {
      return state.message
    }
  }
}
