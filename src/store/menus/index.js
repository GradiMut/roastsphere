import * as firebase from 'firebase'

export default {
  state: {
    foodSize: null,
    desertSize: null,
    baverageSize: null,
    foodItems: [],
    dessertItems: [],
    bevItems: []
  },
  mutations: {
    setFoodSize (state, payload) {
      state.foodSize = payload
    },
    setDesertSize (state, payload) {
      state.desertSize = payload
    },
    setBaverageSize (state, payload) {
      state.baverageSize = payload
    },
    setFoodItems (state, payload) {
      state.foodItems = payload
    },
    setDessertItems (state, payload) {
      state.dessertItems = payload
    },
    setBevItems (state, payload) {
      state.bevItems = payload
    }
  },
  actions: {
    addFood ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')
      let image
      let key
      const food = {
        name: payload.name,
        price: payload.price,
        description: payload.description
      }
      firebase.firestore().collection('foods').doc(payload.foodCategory).collection('menus').add(food).then((docRef) => {
        key = docRef.id
        return key
      })
        .then(key => {
          const filename = payload.imageUrl.name
          const ext = filename.slice(filename.lastIndexOf('.'))
          firebase.storage().ref('foodImages/' + key + '.' + ext).put(payload.imageUrl)
            .then(snapshot => {
              return snapshot.ref.getDownloadURL()
            })
            .then(downloadUrls => {
              image = downloadUrls
              firebase.firestore().collection('foods').doc(payload.foodCategory).collection('menus').doc(key).update({
                imageUrl: image
              }).then(
                commit('setLoading', false)
              ).catch((error) => {
                commit('setLoading', false)
                commit('setError', error)
                console.log('Error 1 : ' + error)
                alert(error)
              })
              alert('Food Added')
            })
        })
        .catch((error) => {
          commit('setLoading', false)
          commit('setError', error)
          alert(error)
        })
    },
    addDessert ({commit}, payload) {
      commit('setLoading', true)
      let image
      let key
      const dessert = {
        name: payload.name,
        price: payload.price,
        description: payload.description
      }
      firebase.firestore().collection('desserts').doc(payload.category).collection('menus').add(dessert).then((docRef) => {
        key = docRef.id
        return key
      }).then(key => {
        const filename = payload.imageUrl.name
        const ext = filename.slice(filename.lastIndexOf('.'))
        firebase.storage().ref('dessertImages/' + key + '.' + ext).put(payload.imageUrl)
          .then(snapshot => {
            return snapshot.ref.getDownloadURL()
          })
          .then(downloadUrls => {
            image = downloadUrls
            firebase.firestore().collection('desserts').doc(payload.category).collection('menus').doc(key).update({
              imageUrl: image
            }).then(
              commit('setLoading', false)
            ).catch((error) => {
              commit('setLoading', false)
              commit('setError', error)
            })
            alert('Dessert Added')
          })
      })
        .catch((error) => {
          commit('setLoading', false)
          commit('setError', error)
        })
    },
    addBaverage ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')
      let image
      let key
      const beverage = {
        name: payload.name,
        price: payload.price,
        description: payload.description
      }
      firebase.firestore().collection('beverages').doc(payload.category).collection('menus').add(beverage).then((docRef) => {
        // console.log('added to firestore, 45%')
        key = docRef.id
        return key
      })
        .then(key => {
          const filename = payload.imageUrl.name
          const ext = filename.slice(filename.lastIndexOf('.'))
          firebase.storage().ref('beverageImages/' + key + '.' + ext).put(payload.imageUrl)
            .then(snapshot => {
              return snapshot.ref.getDownloadURL()
            })
            .then(downloadUrls => {
              image = downloadUrls
              firebase.firestore().collection('beverages').doc(payload.category).collection('menus').doc(key).update({
                imageUrl: image
              }).then(
                commit('setLoading', false)
              ).catch((error) => {
                commit('setLoading', false)
                commit('setError', error)
              })
              alert('Baverage Added')
            })
        })
        .catch((error) => {
          commit('setLoading', false)
          commit('setError', error)
        })
    },
    updateFood ({commit}, payload) {
      commit('setLoading', true)
      const updateData = {}
      if (payload.imageUrl === null) {
        if (payload.name) {
          updateData.name = payload.name
        }
        if (payload.price) {
          updateData.price = payload.price
        }
        if (payload.description) {
          updateData.description = payload.description
        }
        firebase.firestore().collection('foods').doc(payload.id).update(updateData).then(() => {
          commit('setLoading', false)
          alert('Food Updated successfull')
        }).catch(error => {
          commit('setLoading', false)
          commit('setError', error)
        })
      } else {
        const filename = payload.imageUrl.name
        const ext = filename.slice(filename.lastIndexOf('.'))
        if (payload.name) {
          updateData.name = payload.name
        }
        if (payload.price) {
          updateData.price = payload.price
        }
        if (payload.description) {
          updateData.description = payload.description
        }
        if (payload.imageUrl) {
          updateData.imageUrl = payload.imageUrl
        }
        firebase.storage().ref('foodImages/' + payload.id + '.' + ext).put(payload.imageUrl)
          .then(snapshot => {
            return snapshot.ref.getDownloadURL()
          })
          .then(downloadUrls => {
            updateData.imageUrl = downloadUrls
            firebase.firestore().collection('foods').doc(payload.id).update(updateData).then(
              commit('setLoading', false)
            ).catch((error) => {
              commit('setLoading', false)
              commit('setError', error)
            })
            alert('Food Updated successfull')
          })
      }

      // firebase.firestore().collection('foods').doc(payload.id)
    },
    updateDessert ({commit}, payload) {
      commit('setLoading', true)
      const updateData = {}
      if (payload.imageUrl === null) {
        if (payload.name) {
          updateData.name = payload.name
        }
        if (payload.price) {
          updateData.price = payload.price
        }
        if (payload.description) {
          updateData.description = payload.description
        }
        if (payload.category) {
          updateData.category = payload.category
        }
        firebase.firestore().collection('desserts').doc(payload.id).update(updateData).then(() => {
          commit('setLoading', false)
          alert('Food Updated successfull')
        }).catch(error => {
          commit('setLoading', false)
          commit('setError', error)
        })
      } else {
        const filename = payload.imageUrl.name
        const ext = filename.slice(filename.lastIndexOf('.'))
        if (payload.name) {
          updateData.name = payload.name
        }
        if (payload.price) {
          updateData.price = payload.price
        }
        if (payload.description) {
          updateData.description = payload.description
        }
        if (payload.category) {
          updateData.category = payload.category
        }
        if (payload.imageUrl) {
          updateData.imageUrl = payload.imageUrl
        }
        firebase.storage().ref('dessertImages/' + payload.id + '.' + ext).put(payload.imageUrl)
          .then(snapshot => {
            return snapshot.ref.getDownloadURL()
          })
          .then(downloadUrls => {
            updateData.imageUrl = downloadUrls
            firebase.firestore().collection('desserts').doc(payload.id).update(updateData).then(
              commit('setLoading', false)
            ).catch((error) => {
              commit('setLoading', false)
              commit('setError', error)
            })
            alert('Food Updated successfull')
          })
      }

      // firebase.firestore().collection('foods').doc(payload.id)
    },
    updateBev ({commit}, payload) {
      commit('setLoading', true)
      const updateData = {}
      if (payload.imageUrl === null) {
        if (payload.name) {
          updateData.name = payload.name
        }
        if (payload.price) {
          updateData.price = payload.price
        }
        if (payload.description) {
          updateData.description = payload.description
        }
        if (payload.category) {
          updateData.category = payload.category
        }
        if (payload.category) {
          updateData.subCategory = payload.subCategory
        }
        firebase.firestore().collection('beverages').doc(payload.id).update(updateData).then(() => {
          commit('setLoading', false)
          alert('Food Updated successfull')
        }).catch(error => {
          commit('setLoading', false)
          commit('setError', error)
        })
      } else {
        const filename = payload.imageUrl.name
        const ext = filename.slice(filename.lastIndexOf('.'))
        if (payload.name) {
          updateData.name = payload.name
        }
        if (payload.price) {
          updateData.price = payload.price
        }
        if (payload.description) {
          updateData.description = payload.description
        }
        if (payload.category) {
          updateData.category = payload.category
        }
        if (payload.category) {
          updateData.subCategory = payload.subCategory
        }
        if (payload.imageUrl) {
          updateData.imageUrl = payload.imageUrl
        }
        firebase.storage().ref('beverageImages/' + payload.id + '.' + ext).put(payload.imageUrl)
          .then(snapshot => {
            return snapshot.ref.getDownloadURL()
          })
          .then(downloadUrls => {
            updateData.imageUrl = downloadUrls
            firebase.firestore().collection('beverages').doc(payload.id).update(updateData).then(
              commit('setLoading', false)
            ).catch((error) => {
              commit('setLoading', false)
              commit('setError', error)
            })
            alert('Food Updated successfull')
          })
      }

      // firebase.firestore().collection('foods').doc(payload.id)
    },
    fetchFoodCategory ({commit}, getters) {
      firebase.firestore().collection('foods').get().then(function (querySnapshot) {
        let foodCategoryItem = []
        querySnapshot.forEach(doc => {
          foodCategoryItem.push(doc.data().name)
        })
        commit('setFoodItems', foodCategoryItem)
      }).catch(error => {
        console.log('Error While getting data', error)
      })
    },
    fetchDessertCategory ({commit}, getters) {
      firebase.firestore().collection('desserts').get().then(function (querySnapshot) {
        let dessertCategoryItem = []
        querySnapshot.forEach(doc => {
          dessertCategoryItem.push(doc.data().name)
        })
        commit('setDessertItems', dessertCategoryItem)
      }).catch(error => {
        console.log('Error While getting data', error)
      })
    },
    fetchBevCategory ({commit}, getters) {
      firebase.firestore().collection('beverages').get().then(function (querySnapshot) {
        let bevCategoryItem = []
        querySnapshot.forEach(doc => {
          bevCategoryItem.push(doc.data().name)
        })
        commit('setBevItems', bevCategoryItem)
      }).catch(error => {
        console.log('Error While getting data', error)
      })
    },
    fetchFoodSize ({commit}, getters) {
      firebase.firestore().collection('counters').doc('foods').onSnapshot(doc => {
        if (doc.exists) {
          const foodSize = doc.data().foodsCount
          commit('setFoodSize', foodSize)
        } else {
          console.log('No documeent found for message')
        }
      })
    },
    fetchDessertSize ({commit}, getters) {
      firebase.firestore().collection('counters').doc('desserts').onSnapshot(doc => {
        if (doc.exists) {
          const dessertSize = doc.data().dessertsCount
          commit('setDesertSize', dessertSize)
        } else {
          console.log('No documeent found for message')
        }
      })
    },
    fetchBeverageSize ({commit}, getters) {
      firebase.firestore().collection('counters').doc('beverages').onSnapshot(doc => {
        const beverageSize = doc.data().beveragesCount
        commit('setBaverageSize', beverageSize)
      })
    }
  },
  getters: {
    foodSize (state) {
      return state.foodSize
    },
    desertSize (state) {
      return state.desertSize
    },
    baverageSize (state) {
      return state.baverageSize
    },
    foodItems (state) {
      return state.foodItems
    },
    dessertItems (state) {
      return state.dessertItems
    },
    bevItems (state) {
      return state.bevItems
    }
  }
}
