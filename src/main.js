// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase'
import 'firebase/firestore'
import { store } from './store'
import Alert from './components/utils/alert.vue'
import {
  Vuetify,
  VApp,
  VNavigationDrawer,
  VFooter,
  VList,
  VBtn,
  VIcon,
  VGrid,
  VToolbar,
  transitions,
  VAvatar,
  VCard,
  VProgressLinear,
  VBottomSheet,
  VSlider,
  VProgressCircular,
  VForm,
  VTextField,
  VDataTable,
  VTooltip,
  VDivider,
  VDialog,
  VSelect,
  VAutocomplete,
  VTextarea,
  VTabs,
  VExpansionPanel,
  VAlert,
  VBadge
} from 'vuetify'
import '../node_modules/vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  components: {
    VApp,
    VNavigationDrawer,
    VFooter,
    VList,
    VBtn,
    VIcon,
    VGrid,
    VToolbar,
    transitions,
    VAvatar,
    VCard,
    VProgressLinear,
    VBottomSheet,
    VSlider,
    VProgressCircular,
    VForm,
    VTextField,
    VDataTable,
    VTooltip,
    VDivider,
    VDialog,
    VSelect,
    VAutocomplete,
    VTextarea,
    VTabs,
    VExpansionPanel,
    VAlert,
    VBadge
  },
  theme: {
    primary: '#332513',
    secondary: '#332513',
    accent: '#f4c242',
    error: '#FF5252',
    red_accent: '#f46841',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
})
var config = {
  apiKey: 'AIzaSyDS_BSHsn8Ep5lz_w7z2EUVVsJHpVnlUXg',
  authDomain: 'roast-sphere.firebaseapp.com',
  databaseURL: 'https://roast-sphere.firebaseio.com',
  projectId: 'roast-sphere',
  storageBucket: 'roast-sphere.appspot.com',
  messagingSenderId: '536105789341'
}
firebase.initializeApp(config)
Vue.config.productionTip = false
Vue.component('app-alert', Alert)
Vue.filter('number', (value) => {
  let number = value + 1
  if (number < 10) {
    return number
  }
  return number
})
Vue.filter('date', (value) => {
  let date = new Date(value)
  return date.toLocaleString(['en-US'], {month: 'short', day: '2-digit', year: 'numeric', hour: '2-digit', minute: '2-digit'})
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  created () {
    firebase.auth().onAuthStateChanged((firebaseUser) => {
      if (firebaseUser) {
        store.dispatch('autoSignIn', firebaseUser)
        // store.dispatch('fetchFoodCategory')
        // store.dispatch('fetchFoodSize')
        // store.dispatch('fetchDessertSize')
        // store.dispatch('fetchBeverageSize')
        // store.dispatch('fetchResrervationSize')
        // store.dispatch('fetchReviewSize')
        // store.dispatch('fetchMessage')
        // store.dispatch('orderSize')
      }
    })
  }
})
